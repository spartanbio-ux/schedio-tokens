/**
 * FontWeightTokens
 */
declare interface FontWeight {
  bold: 700;
  demi: 600;
  light: 300;
  regular: 400;
}

declare const tokens: FontWeight;
export = tokens;
