/**
 * BorderWidthTokens
 */
declare interface BorderWidth {
  base: '1px';
  thick: '2px';
  thickest: '4px';
}

declare const tokens: BorderWidth;
export = tokens;
