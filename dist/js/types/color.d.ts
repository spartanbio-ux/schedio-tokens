/**
 * ColorTokens
 */
declare interface Color {
  turquoiseLight: 'rgb(71, 202, 245)';
  goldLighter: 'rgb(254, 226, 170)';
  ice: 'rgb(242, 245, 247)';
  greenLight: 'rgb(25, 169, 83)';
  magentaDark: 'rgb(158, 0, 79)';
  blue: 'rgb(0, 76, 153)';
  nightLighter: 'rgb(98, 122, 147)';
  goldDarker: 'rgb(204, 129, 0)';
  greyDarker: 'rgb(101, 117, 134)';
  red: 'rgb(234, 16, 16)';
  magentaLight: 'rgb(250, 97, 173)';
  redDarker: 'rgb(76, 5, 5)';
  grey: 'rgb(184, 194, 204)';
  gold: 'rgb(252, 186, 54)';
  greenDark: 'rgb(3, 99, 41)';
  turquoiseDark: 'rgb(0, 122, 163)';
  turquoiseLighter: 'rgb(148, 224, 249)';
  blueLight: 'rgb(0, 107, 214)';
  night: 'rgb(33, 43, 54)';
  nightLightest: 'rgb(151, 168, 186)';
  greenLighter: 'rgb(41, 214, 110)';
  redLight: 'rgb(244, 102, 102)';
  magentaDarker: 'rgb(82, 0, 41)';
  greyLight: 'rgb(214, 219, 225)';
  goldLight: 'rgb(253, 208, 119)';
  nightDark: 'rgb(14, 18, 22)';
  magentaLighter: 'rgb(250, 178, 214)';
  turquoise: 'rgb(0, 180, 240)';
  greenDarker: 'rgb(1, 60, 25)';
  nightLight: 'rgb(64, 84, 104)';
  green: 'rgb(6, 137, 58)';
  goldDark: 'rgb(255, 170, 0)';
  blueLightest: 'rgb(100, 173, 247)';
  turquoiseDarker: 'rgb(0, 69, 92)';
  greyDark: 'rgb(139, 153, 167)';
  blueLighter: 'rgb(37, 138, 239)';
  redDark: 'rgb(148, 10, 10)';
  redLighter: 'rgb(250, 179, 179)';
  magenta: 'rgb(230, 0, 115)';
  white: 'rgb(255, 255, 255)';
  greyLighter: 'rgb(231, 235, 238)';
  blueDark: 'rgb(0, 36, 71)';
}

declare const tokens: Color;
export = tokens;
