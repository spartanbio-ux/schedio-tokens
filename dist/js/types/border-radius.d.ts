/**
 * BorderRadiusTokens
 */
declare interface BorderRadius {
  small: '0.25rem';
  base: '0.5rem';
  circle: '50%';
}

declare const tokens: BorderRadius;
export = tokens;
