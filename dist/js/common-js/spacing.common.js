/**
 * spacing tokens
 */
module.exports = {
  none: 0,
  quarter: '0.25rem',
  triple: '3rem',
  loose: '1.5rem',
  tight: '0.75rem',
  quadruple: '4rem',
  double: '2rem',
  base: '1rem',
  half: '0.5rem',
}
