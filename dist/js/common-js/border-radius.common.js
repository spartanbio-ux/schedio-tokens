/**
 * border-radius tokens
 */
module.exports = {
  small: '0.25rem',
  base: '0.5rem',
  circle: '50%',
}
