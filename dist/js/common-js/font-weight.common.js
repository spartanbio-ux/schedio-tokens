/**
 * font-weight tokens
 */
module.exports = {
  bold: 700,
  demi: 600,
  light: 300,
  regular: 400,
}
