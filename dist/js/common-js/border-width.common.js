/**
 * border-width tokens
 */
module.exports = {
  base: '1px',
  thick: '2px',
  thickest: '4px',
}
