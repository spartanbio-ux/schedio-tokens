/**
 * line-height tokens
 */
module.exports = {
  base: '150%',
  largeText: '125%',
}
