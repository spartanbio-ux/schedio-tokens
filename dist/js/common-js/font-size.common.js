/**
 * font-size tokens
 */
module.exports = {
  base: '16px',
  size1: '5rem',
  size2: '3.375rem',
  size3: '2.25rem',
  size4: '1.5rem',
  size5: '1rem',
  size6: '0.75rem',
  size7: '0.5rem',
}
