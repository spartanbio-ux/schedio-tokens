/**
 * duration tokens
 */
module.exports = {
  none: 0,
  fast: 100,
  base: 200,
  slow: 300,
  slower: 400,
  slowest: 500,
}
