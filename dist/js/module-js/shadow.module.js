/**
 * shadow tokens
 */

/* Shadow values are intended to be used with colors added to them */
export const border = '0 0 1px'
/* Shadow values are intended to be used with colors added to them */
export const light = '0 0.25rem 1rem'
/* Shadow values are intended to be used with colors added to them */
export const base = '0 0.25rem 1rem'
/* Shadow values are intended to be used with colors added to them */
export const heavy = '0 0.5rem 1.5rem 0.25rem'
