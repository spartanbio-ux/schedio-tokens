/**
 * All tokens
 */

export const borderRadiusSmall = '0.25rem'
export const borderRadiusBase = '0.5rem'
export const borderRadiusCircle = '50%'
export const borderWidthBase = '1px'
export const borderWidthThick = '2px'
export const borderWidthThickest = '4px'
export const colorTurquoiseLight = 'rgb(71, 202, 245)'
export const colorGoldLighter = 'rgb(254, 226, 170)'
export const colorIce = 'rgb(242, 245, 247)'
export const colorGreenLight = 'rgb(25, 169, 83)'
export const colorMagentaDark = 'rgb(158, 0, 79)'
export const colorBlue = 'rgb(0, 76, 153)'
export const colorNightLighter = 'rgb(98, 122, 147)'
export const colorGoldDarker = 'rgb(204, 129, 0)'
export const colorGreyDarker = 'rgb(101, 117, 134)'
export const colorRed = 'rgb(234, 16, 16)'
export const colorMagentaLight = 'rgb(250, 97, 173)'
export const colorRedDarker = 'rgb(76, 5, 5)'
export const colorGrey = 'rgb(184, 194, 204)'
export const colorGold = 'rgb(252, 186, 54)'
export const colorGreenDark = 'rgb(3, 99, 41)'
export const colorTurquoiseDark = 'rgb(0, 122, 163)'
export const colorTurquoiseLighter = 'rgb(148, 224, 249)'
export const colorBlueLight = 'rgb(0, 107, 214)'
export const colorNight = 'rgb(33, 43, 54)'
export const colorNightLightest = 'rgb(151, 168, 186)'
export const colorGreenLighter = 'rgb(41, 214, 110)'
export const colorRedLight = 'rgb(244, 102, 102)'
export const colorMagentaDarker = 'rgb(82, 0, 41)'
export const colorGreyLight = 'rgb(214, 219, 225)'
export const colorGoldLight = 'rgb(253, 208, 119)'
export const colorNightDark = 'rgb(14, 18, 22)'
export const colorMagentaLighter = 'rgb(250, 178, 214)'
export const colorTurquoise = 'rgb(0, 180, 240)'
export const colorGreenDarker = 'rgb(1, 60, 25)'
export const colorNightLight = 'rgb(64, 84, 104)'
export const colorGreen = 'rgb(6, 137, 58)'
export const colorGoldDark = 'rgb(255, 170, 0)'
export const colorBlueLightest = 'rgb(100, 173, 247)'
export const colorTurquoiseDarker = 'rgb(0, 69, 92)'
export const colorGreyDark = 'rgb(139, 153, 167)'
export const colorBlueLighter = 'rgb(37, 138, 239)'
export const colorRedDark = 'rgb(148, 10, 10)'
export const colorRedLighter = 'rgb(250, 179, 179)'
export const colorMagenta = 'rgb(230, 0, 115)'
export const colorWhite = 'rgb(255, 255, 255)'
export const colorGreyLighter = 'rgb(231, 235, 238)'
export const colorBlueDark = 'rgb(0, 36, 71)'
export const durationNone = 0
export const durationFast = 100
export const durationBase = 200
export const durationSlow = 300
export const durationSlower = 400
export const durationSlowest = 500
export const easingBase = [
  0.64,
  0,
  0.35,
  1,
]
export const easingEaseIn = [
  0.36,
  0,
  1,
  1,
]
export const easingEaseOut = [
  0,
  0,
  0.42,
  1,
]
export const easingExcite = [
  0.18,
  0.67,
  0.6,
  1.22,
]
export const easingOvershoot = [
  0.07,
  0.28,
  0.32,
  1.22,
]
export const easingAnticipate = [
  0.38,
  -0.4,
  0.88,
  0.65,
]
export const fontFamilySansSerif = "'Avenir Next', -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif"
export const fontFamilyMonospaced = "'SFMono-Regular', Consolas, 'Fira Sans', 'Liberation Mono', Menlo, 'Courier New', Courier, monospace"
export const fontSizeBase = '16px'
export const fontSize1 = '5rem'
export const fontSize2 = '3.375rem'
export const fontSize3 = '2.25rem'
export const fontSize4 = '1.5rem'
export const fontSize5 = '1rem'
export const fontSize6 = '0.75rem'
export const fontSize7 = '0.5rem'
export const fontWeightBold = 700
export const fontWeightDemi = 600
export const fontWeightLight = 300
export const fontWeightRegular = 400
export const lineHeightBase = '150%'
export const lineHeightLargeText = '125%'
/* Shadow values are intended to be used with colors added to them */
export const shadowBorder = '0 0 1px'
/* Shadow values are intended to be used with colors added to them */
export const shadowLight = '0 0.25rem 1rem'
/* Shadow values are intended to be used with colors added to them */
export const shadowBase = '0 0.25rem 1rem'
/* Shadow values are intended to be used with colors added to them */
export const shadowHeavy = '0 0.5rem 1.5rem 0.25rem'
export const spacingNone = 0
export const spacingQuarter = '0.25rem'
export const spacingTriple = '3rem'
export const spacingLoose = '1.5rem'
export const spacingTight = '0.75rem'
export const spacingQuadruple = '4rem'
export const spacingDouble = '2rem'
export const spacingBase = '1rem'
export const spacingHalf = '0.5rem'
