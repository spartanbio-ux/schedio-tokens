/**
 * line-height tokens
 */

export const base = '150%'
export const largeText = '125%'
