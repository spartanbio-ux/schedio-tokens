/**
 * border-radius tokens
 */

export const small = '0.25rem'
export const base = '0.5rem'
export const circle = '50%'
