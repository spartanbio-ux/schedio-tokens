/**
 * font-size tokens
 */

export const base = '16px'
export const size1 = '5rem'
export const size2 = '3.375rem'
export const size3 = '2.25rem'
export const size4 = '1.5rem'
export const size5 = '1rem'
export const size6 = '0.75rem'
export const size7 = '0.5rem'
