/**
 * easing tokens
 */

export const base = [
  0.64,
  0,
  0.35,
  1,
]
export const easeIn = [
  0.36,
  0,
  1,
  1,
]
export const easeOut = [
  0,
  0,
  0.42,
  1,
]
export const excite = [
  0.18,
  0.67,
  0.6,
  1.22,
]
export const overshoot = [
  0.07,
  0.28,
  0.32,
  1.22,
]
export const anticipate = [
  0.38,
  -0.4,
  0.88,
  0.65,
]
