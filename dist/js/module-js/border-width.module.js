/**
 * border-width tokens
 */

export const base = '1px'
export const thick = '2px'
export const thickest = '4px'
