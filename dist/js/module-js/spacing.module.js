/**
 * spacing tokens
 */

export const none = 0
export const quarter = '0.25rem'
export const triple = '3rem'
export const loose = '1.5rem'
export const tight = '0.75rem'
export const quadruple = '4rem'
export const double = '2rem'
export const base = '1rem'
export const half = '0.5rem'
