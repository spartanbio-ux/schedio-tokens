/**
 * duration tokens
 */

export const none = 0
export const fast = 100
export const base = 200
export const slow = 300
export const slower = 400
export const slowest = 500
