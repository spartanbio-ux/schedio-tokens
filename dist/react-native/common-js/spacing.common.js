/**
 * spacing tokens
 */
module.exports = {
  none: 0,
  quarter: 4,
  triple: 48,
  loose: 24,
  tight: 12,
  quadruple: 64,
  double: 32,
  base: 16,
  half: 8,
}
