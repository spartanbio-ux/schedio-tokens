/**
 * border-width tokens
 */
module.exports = {
  base: 1,
  thick: 2,
  thickest: 4,
}
