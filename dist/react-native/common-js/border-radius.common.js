/**
 * border-radius tokens
 */
module.exports = {
  small: 4,
  base: 8,
  circle: '50%',
}
