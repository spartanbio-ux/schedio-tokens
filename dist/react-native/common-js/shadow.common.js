/**
 * shadow tokens
 */
module.exports = {
  /* Shadow values are intended to be used with colors added to them */
  border: {
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 1,
  },
  /* Shadow values are intended to be used with colors added to them */
  light: {
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 16,
  },
  /* Shadow values are intended to be used with colors added to them */
  base: {
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 16,
  },
  /* Shadow values are intended to be used with colors added to them */
  heavy: {
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowRadius: 24,
  },
}
