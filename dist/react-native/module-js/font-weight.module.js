/**
 * font-weight tokens
 */

export const bold = 700
export const demi = 600
export const light = 300
export const regular = 400
