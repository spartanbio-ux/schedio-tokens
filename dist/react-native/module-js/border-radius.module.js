/**
 * border-radius tokens
 */

export const small = 4
export const base = 8
export const circle = '50%'
