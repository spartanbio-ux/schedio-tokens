/**
 * font-size tokens
 */

export const base = 16
export const size1 = 80
export const size2 = 54
export const size3 = 36
export const size4 = 24
export const size5 = 16
export const size6 = 12
export const size7 = 8
