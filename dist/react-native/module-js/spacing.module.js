/**
 * spacing tokens
 */

export const none = 0
export const quarter = 4
export const triple = 48
export const loose = 24
export const tight = 12
export const quadruple = 64
export const double = 32
export const base = 16
export const half = 8
