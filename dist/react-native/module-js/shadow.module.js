/**
 * shadow tokens
 */

/* Shadow values are intended to be used with colors added to them */
export const border = {
  shadowOffset: {
    width: 0,
    height: 0,
  },
  shadowRadius: 1,
}
/* Shadow values are intended to be used with colors added to them */
export const light = {
  shadowOffset: {
    width: 0,
    height: 4,
  },
  shadowRadius: 16,
}
/* Shadow values are intended to be used with colors added to them */
export const base = {
  shadowOffset: {
    width: 0,
    height: 4,
  },
  shadowRadius: 16,
}
/* Shadow values are intended to be used with colors added to them */
export const heavy = {
  shadowOffset: {
    width: 0,
    height: 8,
  },
  shadowRadius: 24,
}
