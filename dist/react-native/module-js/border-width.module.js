/**
 * border-width tokens
 */

export const base = 1
export const thick = 2
export const thickest = 4
