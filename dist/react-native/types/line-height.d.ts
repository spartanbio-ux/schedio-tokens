/**
 * LineHeightTokens
 */
declare interface LineHeight {
  base: '150%';
  largeText: '125%';
}

declare const tokens: LineHeight;
export = tokens;
