/**
 * BorderWidthTokens
 */
declare interface BorderWidth {
  base: 1;
  thick: 2;
  thickest: 4;
}

declare const tokens: BorderWidth;
export = tokens;
