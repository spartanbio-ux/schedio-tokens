/**
 * FontSizeTokens
 */
declare interface FontSize {
  base: 16;
  size1: 80;
  size2: 54;
  size3: 36;
  size4: 24;
  size5: 16;
  size6: 12;
  size7: 8;
}

declare const tokens: FontSize;
export = tokens;
