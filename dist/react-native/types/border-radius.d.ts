/**
 * BorderRadiusTokens
 */
declare interface BorderRadius {
  small: 4;
  base: 8;
  circle: '50%';
}

declare const tokens: BorderRadius;
export = tokens;
